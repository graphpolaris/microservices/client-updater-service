# client-updater-service

To run this, you'll first need to link the ts-common package to the this package. To do this, first make sure you have it in your workspace:

Pull the latest changes of development-script:

```bash
git pull
```

And pull the latest changes of ts-common:

```bash
cd dependencies/ts-common
git pull
```

(you can also run the commands of the monorepo to pull ALL dependencies, it also works)

Then use the proper make command to link the ts-common package to this one:

```bash
make link
```

To run:

```bash
make develop
```

or (in this alternative, you need to run `bun install` first)

```bash
bun run dev
```
