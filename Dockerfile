# use the official Bun image
# see all versions at https://hub.docker.com/r/oven/bun/tags
FROM oven/bun:alpine
WORKDIR /usr/src/app

# install dependencies into temp directory
# this will cache them and speed up future builds
RUN apk add --no-cache git

RUN mkdir /app
WORKDIR /app

COPY package.json bun.lockb index.ts ./
COPY ./src ./src

RUN sed -i 's|link:ts-common|git+https://git.science.uu.nl/graphpolaris/ts-common.git|' ./package.json
RUN bun install

# copy production dependencies and source code into final image

# run the app
USER bun
ENTRYPOINT [ "bun", "run", "--production", "src/index.ts" ]