import type { ServerWebSocket } from 'bun';
import type { SessionData } from 'ts-common';
import type { RabbitMqBroker } from 'ts-common/rabbitMq';
import type { RedisConnector } from 'ts-common/redis';

export type WebSocketData = {
  sessionData: SessionData;
  saveStateId?: string;
};

export type ServerWebSocketType = ServerWebSocket<WebSocketData>;
export type ClientPool = Map<string, ServerWebSocketType>;
export type WsParams = {
  ws: ServerWebSocketType;
  clientPool: ClientPool;
  redis: RedisConnector;
  rabbit: RabbitPublisherMap;
};

export type RabbitPublisherMap = {
  query: RabbitMqBroker;
  schema: RabbitMqBroker;
};
