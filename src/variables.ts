import { UMSApi } from 'ts-common';
import { RabbitMqConnection } from 'ts-common/rabbitMq';
import { RedisConnector } from 'ts-common/redis';
import { v4 as uuidv4 } from 'uuid';

export type QueryExecutionTypes = 'neo4j';

export const CACHE_KEY_PREFIX = Bun.env.CACHE_KEY_PREFIX || 'cached-queries';
export const CACHE_DURATION = Bun.env.CACHE_DURATION ? parseInt(Bun.env.CACHE_DURATION) : 300000;
export const USER_MANAGEMENT_SERVICE_API = Bun.env.USER_MANAGEMENT_SERVICE_API || 'http://localhost:8000';
export const DEV = (Bun.env.DEV || 'false') !== 'false';
export const ENV = Bun.env.ENV || 'develop';
export const RABBIT_USER = Bun.env.RABBIT_USER || 'rabbitmq';
export const RABBIT_PASSWORD = Bun.env.RABBIT_PASSWORD || 'DevOnlyPass';
export const RABBIT_HOST = Bun.env.RABBIT_HOST || 'localhost';
export const RABBIT_PORT = parseInt(Bun.env.RABBIT_PORT || '5672');
export const LOG_MESSAGES = Bun.env.LOG_MESSAGES || false;
export const LOG_LEVEL = parseInt(Bun.env.LOG_LEVEL || '1');
export const REDIS_HOST = Bun.env.REDIS_HOST || 'localhost';
export const REDIS_PORT = parseInt(Bun.env.REDIS_PORT || '6379');
export const REDIS_PASSWORD = Bun.env.REDIS_PASSWORD || 'DevOnlyPass';
export const REDIS_SCHEMA_CACHE_DURATION = parseInt(Bun.env.REDIS_SCHEMA_CACHE_DURATION || '3600');
export const QUERY_RETRIEVAL_DURATION = parseInt(Bun.env.QUERY_RETRIEVAL_DURATION || '60');
export const SCHEMA_RETRIEVAL_DURATION = parseInt(Bun.env.SCHEMA_RETRIEVAL_DURATION || '60');
export const ROUTING_RETRIEVAL_DURATION = parseInt(Bun.env.ROUTING_RETRIEVAL_DURATION || '60');
export const PERIOD_PING = Bun.env.PERIOD_PING || '1s';
export const PERIOD_PONG_WAIT = Bun.env.PERIOD_PONG_WAIT || '10s';
export const PERIOD_WRITE_WAIT = Bun.env.PERIOD_WRITE_WAIT || '10s';
export const JWT_SECRET = Bun.env.JWT_SECRET || 'jwt-secret';
export const ALLOWED_ORIGINS = Bun.env.ALLOWED_ORIGINS || 'http://localhost:4200';
export const RUN_INSIGHT_PROCESSOR = (Bun.env.RUN_INSIGHT_PROCESSOR || 'false') !== 'false';
export const RUN_INSIGHT_PROCESSOR_CRON = Bun.env.RUN_INSIGHT_PROCESSOR_CRON || '0 */10 * * * *';
export const RUN_INSIGHT_PROCESSOR_TIMEOUT = parseInt(Bun.env.RUN_INSIGHT_PROCESSOR_TIMEOUT || '5');
export const RUN_INSIGHT_PROCESSOR_LOCK = (Bun.env.RUN_INSIGHT_PROCESSOR_LOCK || 'true') !== 'false';

export const ums = new UMSApi(USER_MANAGEMENT_SERVICE_API);
export const ROUTING_KEY = `routing-key-${uuidv4()}`;
export const QUEUE_NAME = `client-updater-queue-${uuidv4()}`;

export const rabbitMq = new RabbitMqConnection({
  protocol: 'amqp',
  hostname: RABBIT_HOST,
  port: RABBIT_PORT,
  username: RABBIT_USER,
  password: RABBIT_PASSWORD,
});

export const redis = new RedisConnector(REDIS_PASSWORD, REDIS_HOST, REDIS_PORT);
