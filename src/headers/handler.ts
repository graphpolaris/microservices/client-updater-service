import type { RedisConnector } from 'ts-common/redis';
import { log } from '../logger';
import { ALLOWED_ORIGINS, DEV, ROUTING_RETRIEVAL_DURATION, ums, USER_MANAGEMENT_SERVICE_API } from '../variables';
import { getSessionDataFromHeader } from './sessionFromHeader';

export const getHeadersHandler = async (req: Request, redis: RedisConnector): Promise<Response> => {
  try {
    const { sessionData: headerSessionData } = await getSessionDataFromHeader(req, !DEV);

    if (!headerSessionData.username) {
      log.error('no username found in header');
      throw new Error('no username found in header');
    }

    const user = await ums.createUserIfNotExistsByName(headerSessionData.username);
    const sessionData = { ...headerSessionData, userID: user.id };

    log.debug({ UserID: req.headers, sessionData }, 'All header data');

    if (!sessionData.userID) {
      log.error('no username found in header');
      throw new Error('no username found in header');
    }

    const res = new Response(
      JSON.stringify({
        userID: user.id,
        username: sessionData.username,
        sessionID: sessionData.sessionID,
        jwt: sessionData.jwt,
      }),
      {
        status: 200,
        headers: {
          Username: sessionData.username,
          Userid: user.id.toString(),
          Sessionid: sessionData.sessionID,
          Jwt: sessionData.jwt,
          'Access-Control-Allow-Origin': ALLOWED_ORIGINS,
          'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, OPTIONS',
          'Access-Control-Allow-Headers':
            'Accept, Authorization, Content-Type, X-CSRF-Token, Access-Control-Allow-Origin, Allow-Origin-Header, userid, sessionid',
          'Access-Control-Expose-Headers': 'Link',
          'Access-Control-Allow-Credentials': 'true',
          'Access-Control-Max-Age': '300',
        },
      },
    );

    log.info({ sessionID: sessionData.sessionID }, 'session created');
    return res;
  } catch (err) {
    log.error(err, 'unexpected error');
    return new Response('unexpected error', { status: 500 });
  }
};
