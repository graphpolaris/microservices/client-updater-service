import { type SessionData } from 'ts-common';
import { log } from '../logger';
import { v4 } from 'uuid';
import { ums } from '../variables';
import { AuthorizationOperationsEnum, UserAuthorizationObjectsEnum } from 'ts-common/src/model/webSocket/policy';

const notFoundError = new Error('not found');

function getFromHeader<T extends string | undefined = string | undefined>(
  req: Request,
  key: string,
  defaultVal: T,
  lookInParams = false,
): [T, boolean] {
  const val = req.headers.get(key);
  if (val) {
    return [val as T, true];
  }
  if (lookInParams) {
    const url = new URL(req.url);
    const queryParamVal = url.searchParams.get(key);
    if (queryParamVal) {
      return [queryParamVal as T, true];
    }
  }
  return [defaultVal, false];
}

export async function getSessionDataFromHeader(req: Request, isProd: boolean): Promise<{ sessionData: SessionData; foundAll: boolean }> {
  if (!isProd) {
    log.debug('Getting session data from headers DEV mode');
  } else {
    log.trace('Getting session data from headers PROD mode');
  }

  const sessionData: SessionData = {
    username: '',
    userID: 1,
    sessionID: v4(),
    jwt: '',
  };

  let foundAll = true;

  if (!isProd) {
    sessionData.username = 'Username';
    sessionData.userID = 1;
    sessionData.jwt = 'JWT';
  }

  const emailHeaderVariable = process.env.USER_HEADER_VARIABLE || 'X-Authentik-Email';
  const [username, foundUsername] = getFromHeader(req, emailHeaderVariable, sessionData.username);
  if (!foundUsername && isProd) {
    log.error('error reading Username/Email', notFoundError);
    throw notFoundError;
  } else if (!foundUsername) {
    foundAll = false;
  }

  const [userID, foundUserId] = getFromHeader(req, 'X-Authentik-Uid', sessionData.userID.toString());
  if (!foundUserId && isProd) {
    log.error('error reading userID', notFoundError);
    throw notFoundError;
  } else if (!foundUserId) {
    foundAll = false;
  }

  const [impersonateID, foundImpersonateId] = getFromHeader(req, 'Impersonateid', sessionData.impersonateID?.toString());
  let impersonateIDMutable = impersonateID;
  log.info('impersonateID', impersonateIDMutable);
  if (!foundImpersonateId) {
    // get from URL parameter
    const url = new URL(req.url);
    impersonateIDMutable = (url.searchParams.get('impersonateID') as string) || '';
    if (impersonateIDMutable !== '') {
      log.debug('Using impersonateID from URL parameter', impersonateIDMutable);
    }
  }

  const [roomID, foundRoomId] = getFromHeader(req, 'RoomID', sessionData.roomID);
  if (!foundRoomId) {
    foundAll = false;
  }

  const [sessionID, foundSessionID] = getFromHeader(req, 'sessionID', sessionData.sessionID, true);
  if (foundSessionID) {
    log.debug('SessionID found in header', sessionID);
  } else {
    log.debug('SessionID not found in header', sessionID, sessionData.sessionID, req.headers, new URL(req.url));
  }

  const [jwt, foundJwt] = getFromHeader(req, 'X-Authentik-Jwt', sessionData.jwt);
  if (!foundJwt && isProd) {
    log.error('error reading JWT', notFoundError);
    throw notFoundError;
  } else if (!foundJwt) {
    foundAll = false;
  }

  sessionData.username = username;
  sessionData.userID = parseInt(userID);
  sessionData.roomID = roomID;
  sessionData.sessionID = sessionID;
  sessionData.jwt = jwt;
  sessionData.impersonateID = impersonateIDMutable ? parseInt(impersonateIDMutable) : undefined;

  if (sessionData.impersonateID && sessionData.impersonateID !== sessionData.userID && sessionData.userID) {
    try {
      const allowed = await ums.enforceUser(sessionData.userID, [
        { obj: UserAuthorizationObjectsEnum.demoUser, act: AuthorizationOperationsEnum.W },
      ]);
      if (!allowed.demoUser.W) {
        log.warn('impersonateID not allowed for user', sessionData.impersonateID);
        throw new Error('impersonateID not allowed');
      }
      log.debug('impersonateID allowed for user', sessionData.impersonateID);
      sessionData.userID = 0;
      sessionData.username = 'demoUser';
    } catch (err) {
      log.error('error getting user', err);
      throw err;
    }
  }

  return { sessionData, foundAll };
}
