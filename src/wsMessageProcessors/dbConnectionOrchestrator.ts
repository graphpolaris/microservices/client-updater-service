import { Logger, type SaveState, type WsSubHandlerMap } from 'ts-common';
import { LOG_LEVEL, ROUTING_KEY, ums } from '../variables';
import type { WsParams } from '../model';

const log = new Logger(LOG_LEVEL as any, 'dbConnectionOrchestrator');

export const dbConnectionOrchestrator: WsSubHandlerMap<'dbConnection', WsParams> = {
  testConnection: async (message, params) => {
    log.debug('testConnection', message);

    await params.rabbit.schema.publishMessageToBackend(
      message.body,
      'connection-tester-request',
      ROUTING_KEY,
      message,
      params.ws.data.sessionData,
    );

    log.info('Published connection test request');
  },
};
