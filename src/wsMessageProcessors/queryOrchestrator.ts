import { defaultQuery, Logger, MLTypesEnum, wsKeys, wsReturnKey, wsSubKeys, type WsSubHandlerMap } from 'ts-common';
import { LOG_LEVEL, ROUTING_KEY, ums } from '../variables';
import type { WsParams } from '../model';
import { AuthorizationOperationsEnum, UserAuthorizationObjectsEnum } from 'ts-common/src/model/webSocket/policy';
import { writeError, wsWrite } from '../wsMsgToFrontend';

const log = new Logger(LOG_LEVEL as any, 'queryOrchestrator');

export const queryOrchestrator: WsSubHandlerMap<'query', WsParams> = {
  get: async (message, params) => {
    log.debug('get query', message);

    const ss = await ums.getUserSaveState(params.ws.data.sessionData.userID, message.body.saveStateID);

    if (ss.userId !== params.ws.data.sessionData.userID) {
      const allowed = await ums.enforceUser(params.ws.data.sessionData.userID, [
        { obj: UserAuthorizationObjectsEnum.savestate, act: AuthorizationOperationsEnum.R },
      ]);

      if (!allowed.savestate.R) {
        log.error('get query', 'unauthorized');
        writeError(params.ws, message.callID, 'unauthorized');
        return;
      }
    }

    await params.rabbit.query.publishMessageToBackend(
      {
        saveStateID: message.body.saveStateID,
        queryID: message.body.queryID,
        ml: {
          [MLTypesEnum.linkPrediction]: { enabled: false },
          [MLTypesEnum.centrality]: { enabled: false },
          [MLTypesEnum.communityDetection]: { enabled: false },
          [MLTypesEnum.shortestPath]: { enabled: false },
        },
        useCached: message.body.useCached,
      },
      'neo4j-query-request',
      ROUTING_KEY,
      message,
      params.ws.data.sessionData,
    );

    log.info('Published query request to backend');
  },
  create: async (message, params) => {
    log.debug('create query', message);

    try {
      const query = await ums.createQuery(params.ws.data.sessionData.userID, message.body.saveStateID, defaultQuery());

      wsWrite(params.ws, {
        callID: message.callID,
        status: 'success',
        type: wsReturnKey.queryAdd,
        value: query,
      });

      log.info('New query created');
    } catch (e) {
      log.error('create query failed', e);
      writeError(params.ws, message.callID, 'error creating query');
      return;
    }
  },
  update: async (message, params) => {
    log.debug('update query', message);

    try {
      const query = await ums.updateQuery(params.ws.data.sessionData.userID, message.body.saveStateID, message.body.query);

      wsWrite(params.ws, {
        callID: message.callID,
        status: 'success',
        type: wsReturnKey.queryUpdate,
        value: query,
      });

      log.info('Query updated');
    } catch (e) {
      log.error('update query failed', e);
      writeError(params.ws, message.callID, 'error updating query');
      return;
    }
  },
  delete: async (message, params) => {
    log.debug('delete query', message);

    try {
      const ret = await ums.deleteQuery(params.ws.data.sessionData.userID, message.body.saveStateID, message.body.queryID);
      wsWrite(params.ws, {
        callID: message.callID,
        status: 'success',
        type: wsReturnKey.queryDelete,
        value: true,
      });

      log.info('Query deleted');
    } catch (e) {
      log.error('delete query failed', e);
      writeError(params.ws, message.callID, 'error deleting query');
      return;
    }
  },
  manual: async (message, params) => {
    log.debug('manual query', message);

    const allowed = await ums.enforceUser(params.ws.data.sessionData.userID, [
      { obj: UserAuthorizationObjectsEnum.savestate, act: AuthorizationOperationsEnum.R },
    ]);

    if (!allowed.savestate.R) {
      log.error('get query', 'unauthorized');
      writeError(params.ws, message.callID, 'unauthorized');
      return;
    }

    await params.rabbit.schema.publishMessageToBackend(
      message,
      'neo4j-manual-query-request',
      ROUTING_KEY,
      message,
      params.ws.data.sessionData,
    );

    log.info('Published manual query request');
  },
};
