import type { WsHandlerMap, WsHandlerMapFlex } from 'ts-common';
import { dbConnectionOrchestrator } from './dbConnectionOrchestrator';
import type { WsParams } from '../model';
import { schemaOrchestrator } from './schemaOrchestrator';
import { queryOrchestrator } from './queryOrchestrator';
import { insightOrchestrator } from './insightOrchestrator';
import { saveStateOrchestrator } from './saveStateOrchestrator';
import { userOrchestrator } from './userOrchestrator';
import { projectOrchestrator } from './projectOrchestrator';

export const handleMessage: WsHandlerMap<WsParams> = {
  dbConnection: dbConnectionOrchestrator,
  schema: schemaOrchestrator,
  query: queryOrchestrator,
  insight: insightOrchestrator,
  state: saveStateOrchestrator,
  broadcastState: {},
  user: userOrchestrator,
  project: projectOrchestrator,
};
