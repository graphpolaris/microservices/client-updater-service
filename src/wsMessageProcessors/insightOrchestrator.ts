import { Logger, wsKeys, wsReturnKey, wsSubKeys, type WsMessageBodyI, type WsSubHandlerMap } from 'ts-common';
import { LOG_LEVEL, ROUTING_KEY, RUN_INSIGHT_PROCESSOR_TIMEOUT, ums } from '../variables';
import type { WsParams } from '../model';
import { wsWrite } from '../wsMsgToFrontend';
import { insightTask } from '../cronScheduler';

const log = new Logger(LOG_LEVEL as any, 'insightOrchestrator');

export const insightOrchestrator: WsSubHandlerMap<'insight', WsParams> = {
  create: async (message, params) => {
    log.debug('create insight', message);

    const userID = params.ws.data.sessionData.userID;
    const insight = await ums.createInsight(userID, message.body.saveStateId, message.body);

    wsWrite(params.ws, {
      callID: message.callID,
      status: 'success',
      type: wsReturnKey.insightResult,
      value: insight,
    });

    log.info('create insight success', message.callID);
  },
  getAll: async (message, params) => {
    log.debug('getAll insight', message);

    const userID = params.ws.data.sessionData.userID;
    const insights = await ums.getInsights(userID, message.body.saveStateId);

    wsWrite(params.ws, {
      callID: message.callID,
      status: 'success',
      type: wsReturnKey.insightResults,
      value: insights,
    });

    log.info('getAll insight success', message.callID);
  },
  update: async (message, params) => {
    log.debug('update insight', message);

    const userID = params.ws.data.sessionData.userID;
    const insight = await ums.updateInsight(userID, message.body.id, message.body.insight);

    if (message.body.generateEmail) {
      insightTask.publishMessage({
        message: JSON.stringify({ insight, force: true }),
        routingKey: 'insight-processor',
        options: {
          expiration: RUN_INSIGHT_PROCESSOR_TIMEOUT * 60 * 1000, // in minutes
        },
      });
    }

    wsWrite(params.ws, {
      callID: message.callID,
      status: 'success',
      type: wsReturnKey.insightResult,
      value: insight,
    });

    log.info('update insight success', message.callID);
  },
  delete: async (message, params) => {
    log.debug('delete insight', message);

    const userID = params.ws.data.sessionData.userID;
    const result = await ums.deleteInsight(userID, message.body.id);

    wsWrite(params.ws, {
      callID: message.callID,
      status: 'success',
      type: wsReturnKey.insightResults,
      value: result,
    });

    log.info('delete insight success', message.callID);
  },
  getAppearances: async (message, params) => {
    log.debug('get appearances', message);

    const userID = params.ws.data.sessionData.userID;
    const insight = await ums.getInsight(userID, String(message.body.insightID));

    insightTask.publishMessageToBackend(
      { insight, force: false, appearances: true },
      'insight-processor',
      ROUTING_KEY,
      message,
      params.ws.data.sessionData,
    );
  },
};
