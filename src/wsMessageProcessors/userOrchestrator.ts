import { Logger, wsReturnKey, type WsSubHandlerMap } from 'ts-common';
import { LOG_LEVEL, ums } from '../variables';
import type { WsParams } from '../model';
import { writeError, wsWrite } from '../wsMsgToFrontend';
import { AuthorizationOperationsEnum, UserAuthorizationObjectsEnum } from 'ts-common/src/model/webSocket/policy';

const log = new Logger(LOG_LEVEL as any, 'userOrchestrator');

export const userOrchestrator: WsSubHandlerMap<'user', WsParams> = {
  getPolicy: async (message, params) => {
    log.info('getPolicy');
    const userID = params.ws.data.sessionData.userID;
    const user = await ums.getUser(userID);
    if (!user) {
      writeError(params.ws, message.callID, 'User not found');
      return;
    }

    const enforceResult = await ums.enforceUser(userID, [
      { obj: UserAuthorizationObjectsEnum.savestate, act: AuthorizationOperationsEnum.R },
      { obj: UserAuthorizationObjectsEnum.savestate, act: AuthorizationOperationsEnum.W },
      { obj: UserAuthorizationObjectsEnum.demoUser, act: AuthorizationOperationsEnum.R },
      { obj: UserAuthorizationObjectsEnum.demoUser, act: AuthorizationOperationsEnum.W },
    ]);

    wsWrite(params.ws, {
      callID: message.callID,
      status: 'success',
      type: wsReturnKey.userPolicy,
      value: enforceResult,
    });
    log.info('getPolicy responded');
    log.debug('getPolicy responded:', enforceResult);
  },
  policyCheck: async (message, params) => {
    log.info('policyCheck', message);

    const userID = params.ws.data.sessionData.userID;
    const user = await ums.getUser(userID);
    if (!user) {
      writeError(params.ws, message.callID, 'User not found');
      return;
    }

    if (!(message.body.object in UserAuthorizationObjectsEnum)) {
      writeError(params.ws, message.callID, 'Object error');
      return;
    }

    if (!(message.body.operation in AuthorizationOperationsEnum)) {
      writeError(params.ws, message.callID, 'Operation error');
      return;
    }

    const allowed = await ums.enforceUser(userID, [
      {
        obj: message.body.object,
        act: message.body.operation,
      },
    ]);

    wsWrite(params.ws, {
      callID: message.callID,
      status: 'success',
      type: wsReturnKey.user,
      value: allowed[message.body.object][message.body.operation],
    });

    log.info('policyCheck responded');
  },
};
