import { Logger, wsReturnKey, type WsSubHandlerMap } from 'ts-common';
import { LOG_LEVEL, ums } from '../variables';
import type { WsParams } from '../model';
import { writeError, wsWrite } from '../wsMsgToFrontend';
import type { ProjectModel, ProjectRequest } from 'ts-common/src/model/webSocket/project';

const log = new Logger(LOG_LEVEL as any, 'projectOrchestrator');

export const projectOrchestrator: WsSubHandlerMap<'project', WsParams> = {
  create: async (message, params) => {
    log.debug('create project', message);
    if (!message || !message.body) {
      log.error('create', 'message is empty');
      return;
    }

    const projectModel: ProjectModel = {
      id: -1,
      name: message.body.name,
      parentId: message.body.parent || null,
      createdAt: new Date().toISOString(),
      updatedAt: new Date().toISOString(),
      saveStates: [],
      children: [],
    };

    try {
      const project = await ums.createProject(params.ws.data.sessionData.userID, projectModel);
      wsWrite(params.ws, {
        callID: message.callID,
        status: 'success',
        type: wsReturnKey.project,
        value: project,
      });
      log.info('create project success', message.callID);
    } catch (error) {
      writeError(params.ws, message.callID, 'Failed to create project');
      log.error('create project failed', error);
    }
  },

  get: async (message, params) => {
    log.debug('get project', message);

    const parentId = message.body?.parentID || null;

    try {
      const projects = await ums.getProjects(params.ws.data.sessionData.userID, parentId);
      wsWrite(params.ws, {
        callID: message.callID,
        status: 'success',
        type: wsReturnKey.projects,
        value: projects,
      });
      log.info('getAll projects success', message.callID);
    } catch (error) {
      writeError(params.ws, message.callID, 'Failed to get projects');
      log.error('getAll projects failed', error);
    }
  },

  getAll: async (message, params) => {
    log.debug('getAll projects', message);

    const parentId = message.body?.parentID || null;

    try {
      const projects = await ums.getProjects(params.ws.data.sessionData.userID, parentId);
      wsWrite(params.ws, {
        callID: message.callID,
        status: 'success',
        type: wsReturnKey.projects,
        value: projects,
      });
      log.info('getAll projects success', message.callID);
    } catch (error) {
      writeError(params.ws, message.callID, 'Failed to get projects');
      log.error('getAll projects failed', error);
    }
  },

  update: async (message, params) => {
    log.debug('update project', message);
    if (!message || !message.body.id) {
      log.error('update', 'invalid project data');
      return;
    }

    try {
      const existingProject = await ums.getProject(message.body.id);
      if (!existingProject) {
        writeError(params.ws, message.callID, 'project not found');
        return;
      }

      const projectToUpdate: ProjectModel = {
        ...existingProject,
        name: message.body.name,
        parentId: message.body.parent || null,
      };

      const project = await ums.updateProject(message.body.id, projectToUpdate);
      wsWrite(params.ws, {
        callID: message.callID,
        status: 'success',
        type: wsReturnKey.project,
        value: project,
      });
      log.info('update project success', message.callID);
    } catch (error) {
      writeError(params.ws, message.callID, 'Failed to update project');
      log.error('update project failed', error);
    }
  },

  delete: async (message, params) => {
    log.debug('delete project', message);
    if (!message || !message.body.id) {
      log.error('delete', 'invalid project id');
      return;
    }

    try {
      const projectToDelete = await ums.getProject(message.body.id);
      if (!projectToDelete) {
        writeError(params.ws, message.callID, 'project not found');
        return;
      }

      const success = await ums.deleteProject(message.body.id);
      if (!success) {
        writeError(params.ws, message.callID, 'Failed to delete project');
        return;
      }

      wsWrite(params.ws, {
        callID: message.callID,
        status: 'success',
        type: wsReturnKey.project,
        value: projectToDelete,
      });
      log.info('delete project success', message.callID);
    } catch (error) {
      writeError(params.ws, message.callID, 'Failed to delete project');
      log.error('delete project failed', error);
    }
  },
};
