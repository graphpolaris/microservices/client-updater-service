import {
  Logger,
  MLTypesEnum,
  wsKeys,
  wsReturnKey,
  wsSubKeys,
  type WsHandlerMap,
  type WsMessageBodyRequest,
  type WsSubHandlerMap,
} from 'ts-common';
import { LOG_LEVEL, ROUTING_KEY, ums } from '../variables';
import type { WsParams } from '../model';
import { writeError, wsWrite } from '../wsMsgToFrontend';
import {
  AuthorizationOperationsEnum,
  SaveStateAuthorizationObjectsEnum,
  UserAuthorizationObjectsEnum,
} from 'ts-common/src/model/webSocket/policy';

const log = new Logger(LOG_LEVEL as any, 'saveStateOrchestrator');

export const saveStateOrchestrator: WsSubHandlerMap<'state', WsParams> = {
  getPolicy: async (message, params) => {
    log.debug('getPolicy', message);
    try {
      if (!message || !message.body.saveStateID) {
        log.error('getPolicy', 'message is empty');
        return;
      }

      const userID = params.ws.data.sessionData.userID;
      const ss = await ums.getUserSaveState(userID, message.body.saveStateID);
      if (!ss) {
        writeError(params.ws, message.callID, 'save state not found');
        log.error('getPolicy', 'save state not found');
        return;
      }

      if (ss.userId === userID) {
        wsWrite(params.ws, {
          callID: message.callID,
          status: 'success',
          type: wsReturnKey.saveStatesModels,
          value: {
            savestate: { R: true, W: true },
            database: { R: true, W: true },
            query: { R: true, W: true },
            visualization: { R: true, W: true },
            schema: { R: true, W: true },
          },
        });
      } else {
        const enforceUser = await ums.enforceUser(userID, [
          { obj: UserAuthorizationObjectsEnum.savestate, act: AuthorizationOperationsEnum.R },
          { obj: UserAuthorizationObjectsEnum.savestate, act: AuthorizationOperationsEnum.W },
        ]);
        const enforceResult = await ums.enforceSaveState(userID, ss.id, [
          { obj: SaveStateAuthorizationObjectsEnum.savestate, act: AuthorizationOperationsEnum.R },
          { obj: SaveStateAuthorizationObjectsEnum.savestate, act: AuthorizationOperationsEnum.W },
          { obj: SaveStateAuthorizationObjectsEnum.database, act: AuthorizationOperationsEnum.R },
          { obj: SaveStateAuthorizationObjectsEnum.database, act: AuthorizationOperationsEnum.W },
          { obj: SaveStateAuthorizationObjectsEnum.query, act: AuthorizationOperationsEnum.R },
          { obj: SaveStateAuthorizationObjectsEnum.query, act: AuthorizationOperationsEnum.W },
          { obj: SaveStateAuthorizationObjectsEnum.visualization, act: AuthorizationOperationsEnum.R },
          { obj: SaveStateAuthorizationObjectsEnum.visualization, act: AuthorizationOperationsEnum.W },
          { obj: SaveStateAuthorizationObjectsEnum.schema, act: AuthorizationOperationsEnum.R },
          { obj: SaveStateAuthorizationObjectsEnum.schema, act: AuthorizationOperationsEnum.W },
        ]);

        wsWrite(params.ws, {
          callID: message.callID,
          status: 'success',
          type: wsReturnKey.saveStatesModels,
          value: {
            savestate: { R: enforceUser.savestate.R && enforceResult.savestate.R, W: enforceUser.savestate.W && enforceResult.savestate.W },
            database: { R: enforceUser.savestate.R && enforceResult.database.R, W: enforceUser.savestate.W && enforceResult.database.W },
            query: { R: enforceUser.savestate.R && enforceResult.query.R, W: enforceUser.savestate.W && enforceResult.query.W },
            visualization: {
              R: enforceUser.savestate.R && enforceResult.visualization.R,
              W: enforceUser.savestate.W && enforceResult.visualization.W,
            },
            schema: { R: enforceUser.savestate.R && enforceResult.schema.R, W: enforceUser.savestate.W && enforceResult.schema.W },
          },
        });
      }

      log.info('ss getPolicy success', message.callID);
    } catch (e) {
      log.error('Error while getting policy', e);
      writeError(params.ws, message.callID, 'error while getting policy');
    }
  },
  create: async (message, params) => {
    log.debug('create', message, params.ws.data.sessionData);
    if (!message) {
      log.error('create', 'message is empty');
      return;
    }

    const userID = params.ws.data.sessionData.userID;
    try {
      const ss = await ums.createUserSaveState(userID, message.body);
      if (!ss) {
        writeError(params.ws, message.callID, 'save state not created');
        log.error('get', 'save state not created');
        return;
      }

      wsWrite(params.ws, {
        callID: message.callID,
        status: 'success',
        type: wsReturnKey.saveState,
        value: ss,
      });

      log.info('create success', message.callID);
    } catch (e) {
      log.error('Error while creating save state', e);
    }
  },
  update: async (message, params) => {
    log.debug('update', message, params.ws.data.sessionData.userID);
    if (!message) {
      log.error('update', 'message is empty');
      return;
    }

    try {
      const userID = params.ws.data.sessionData.userID;
      const currentSS = await ums.getUserSaveState(userID, message.body.id);

      // Fix inconsistencies to prevent infinite reload loop for the same save state
      message.body.updatedAt = currentSS.updatedAt;
      // - These inconsistently appear and reappear in the output. Shouldnt trigger a reload
      delete currentSS.visualizations.id;
      delete message.body.visualizations.id;
      if (currentSS.schemas != null && currentSS.schemas.length > 0) delete currentSS.schemas[0].id;
      if (message.body.schemas != null && message.body.schemas.length > 0) delete message.body.schemas[0].id;

      // - Omit frontend specific keys such as ignoreReactivity and queryTranslation
      message.body.queryStates.openQueryArray = message.body.queryStates.openQueryArray.map(query => ({
        id: query.id,
        name: query.name,
        graph: query.graph,
        settings: query.settings,
        attributesBeingShown: query.attributesBeingShown,
      }));

      if (Bun.deepEquals(currentSS, message.body)) {
        wsWrite(params.ws, {
          callID: message.callID,
          status: 'unchanged',
          type: wsReturnKey.saveState,
          value: currentSS,
        });
        log.info('update', 'no changes');
        return;
      } else {
        // const diff = Object.keys(message.body).reduce((acc, key) => {
        //   if (!Bun.deepEquals(currentSS[key], message.body[key])) {
        //     acc[key] = { old: currentSS[key], new: message.body[key] };
        //   }
        //   return acc;
        // }, {});
        // log.debug('update differences', diff);
      }

      if (!message.body.queryStates.openQueryArray || message.body.queryStates.openQueryArray.length === 0) {
        writeError(params.ws, message.callID, 'no queries in save state');
        log.error('update', 'no queries in save state', 'aborting');
        return;
      }

      const ss = await ums.updateUserSaveState(userID, message.body);
      if (!ss) {
        writeError(params.ws, message.callID, 'save state not updated');
        log.error('update', 'save state not updated');
        return;
      }

      wsWrite(params.ws, {
        callID: message.callID,
        status: 'success',
        type: wsReturnKey.saveState,
        value: ss,
      });

      log.info('updated save state successful', message.callID);
    } catch (e) {
      log.error('Error while performing update', e);
    }
  },
  get: async (message, params) => {
    log.debug('get', message);

    if (!message || !message.body.saveStateID) {
      log.error('get', 'message is empty');
      return;
    }

    try {
      const userID = params.ws.data.sessionData.userID;
      const ss = await ums.getUserSaveState(userID, message.body.saveStateID);
      if (!ss) {
        writeError(params.ws, message.callID, 'save state not found');
        log.error('get', 'save state not found');
        return;
      }

      wsWrite(params.ws, {
        callID: message.callID,
        status: 'success',
        type: wsReturnKey.saveState,
        value: ss,
      });
      log.info('get success', message.callID);
    } catch (e) {
      log.error('Error while getting save state', e);
      writeError(params.ws, message.callID, 'error while getting save state');
    }
  },
  getAll: async (message, params) => {
    log.info('getAll', message);
    if (!message) {
      log.error('getAll', 'message is empty');
      return;
    }

    try {
      const userID = params.ws.data.sessionData.userID;
      const ss = await ums.getUserSaveStates(userID);
      if (!ss) {
        writeError(params.ws, message.callID, 'user not found');
        log.error('get', 'save state not found');
        return;
      }

      wsWrite(params.ws, {
        callID: message.callID,
        status: 'success',
        type: wsReturnKey.saveStates,
        value: ss,
      });

      log.info('getAll success');
    } catch (e) {
      log.error('Error while getting save states', e);
      writeError(params.ws, message.callID, 'error while getting save states');
    }
  },
  delete: async (message, params) => {
    log.debug('delete', message);

    if (!message || !message.body.saveStateID) {
      log.error('delete', 'message is empty');
      return;
    }

    try {
      const userID = params.ws.data.sessionData.userID;
      const ss = await ums.deleteUserSaveState(userID, message.body.saveStateID);
      if (!ss) {
        writeError(params.ws, message.callID, 'save state not deleted');
        log.error('delete', 'save state not deleted');
        return;
      }

      wsWrite(params.ws, {
        callID: message.callID,
        status: 'success',
        type: wsReturnKey.deleteSaveState,
        value: 'deleted',
      });

      log.info('delete success', message.callID);
    } catch (e) {
      log.error('Error while deleting save state', e);
      writeError(params.ws, message.callID, 'error while deleting save state');
    }
  },
  testConnection: async (message, params) => {
    log.info('test savestate Connection', message);
    if (!message) {
      log.error('testConnection', 'message is empty');
      return;
    }

    try {
      const ss = await ums.getUserSaveState(params.ws.data.sessionData.userID, message.body.saveStateID);
      if (!ss) {
        writeError(params.ws, message.callID, 'save state not found');
        log.error('testConnection', 'save state not found');
        return;
      }

      await params.rabbit.schema.publishMessageToBackend(ss, 'connection-tester-request', ROUTING_KEY, message, params.ws.data.sessionData);
    } catch (e) {
      log.error('Error while testing connection', e);
      writeError(params.ws, message.callID, 'error while testing connection');
    }
  },
  shareState: async (message, params) => {
    log.info('shareState', message);
    if (!message) {
      log.error('shareState', 'message is empty');
      return;
    }

    try {
      const userID = params.ws.data.sessionData.userID;
      const ss = await ums.getUserSaveState(userID, message.body.saveStateId);
      if (!ss) {
        writeError(params.ws, message.callID, 'save state not found');
        log.error('shareState', 'save state not found');
        return;
      }

      if (userID !== ss.userId) {
        writeError(params.ws, message.callID, 'only owner can share save state');
        log.error('shareState', 'only owner can share save state');
        return;
      }

      const shared = await ums.shareSaveState(userID, message.body);

      wsWrite(params.ws, {
        callID: message.callID,
        status: 'success',
        type: wsReturnKey.shareSaveState,
        value: true,
      });
    } catch (e) {
      log.error('Error while sharing save state', e);
      writeError(params.ws, message.callID, 'error while sharing save state');
    }
  },
};
