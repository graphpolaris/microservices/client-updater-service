import { Logger, type BackendMessageHeader, type WsSubHandlerMap } from 'ts-common';
import { LOG_LEVEL, ROUTING_KEY, ums } from '../variables';
import type { WsParams } from '../model';
import { AuthorizationOperationsEnum, SaveStateAuthorizationObjectsEnum } from 'ts-common/src/model/webSocket/policy';
import { writeError } from '../wsMsgToFrontend';

const log = new Logger(LOG_LEVEL as any, 'schemaOrchestrator');

export const schemaOrchestrator: WsSubHandlerMap<'schema', WsParams> = {
  get: async (message, params) => {
    log.debug('get schema', message);

    const ss = await ums.getUserSaveState(params.ws.data.sessionData.userID, message.body.saveStateID);
    if (!ss) {
      log.error('get schema', 'save state not found');
      writeError(params.ws, message.callID, 'save state not found');
      return;
    } // TODO: make this more performant by not needing to fetch ss

    if (ss.userId !== params.ws.data.sessionData.userID) {
      const enforceResult = await ums.enforceSaveState(params.ws.data.sessionData.userID, message.body.saveStateID, [
        { obj: SaveStateAuthorizationObjectsEnum.schema, act: AuthorizationOperationsEnum.R },
      ]);
      if (!enforceResult.schema.R) {
        log.error('get schema', 'unauthorized');
        writeError(params.ws, message.callID, 'unauthorized');
        return;
      }
    }

    params.rabbit.schema.publishMessageToBackend(message.body, 'neo4j-schema-request', ROUTING_KEY, message, params.ws.data.sessionData);
    log.info('Published schema request');
  },
  getSchemaStats: (message, params) => {
    log.debug('getSchemaStats schema', message);
    // TODO: implement
  },
};
