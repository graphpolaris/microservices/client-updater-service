import { CronJob } from 'cron';
import {
  rabbitMq,
  redis,
  RUN_INSIGHT_PROCESSOR,
  RUN_INSIGHT_PROCESSOR_CRON,
  RUN_INSIGHT_PROCESSOR_LOCK,
  RUN_INSIGHT_PROCESSOR_TIMEOUT,
  ums,
} from '../variables';
import { log } from '../logger';
import { parseExpression } from 'cron-parser';
import { RabbitMqBroker } from 'ts-common/rabbitMq';

const CronOptions = {
  tz: 'Europe/Berlin', // location of Azure server
};

export const insightTask = await new RabbitMqBroker(rabbitMq, 'insight-processor').connect();

export const startCronJob = async () => {
  if (!RUN_INSIGHT_PROCESSOR) {
    log.warn('Insight processor is disabled');
    return;
  }

  log.info('Starting insight processor cron job with cron', RUN_INSIGHT_PROCESSOR_CRON);
  CronJob.from({
    cronTime: RUN_INSIGHT_PROCESSOR_CRON,
    onTick: async function () {
      if (RUN_INSIGHT_PROCESSOR_LOCK) {
        // sleep for a little bit to stagger the cron jobs among instances
        await new Promise(resolve => setTimeout(resolve, Math.floor(Math.random() * 1000 * RUN_INSIGHT_PROCESSOR_TIMEOUT)));
      }

      const [hasLock, release] = await redis.acquireMutex('cron-insight-processor-mutex', {
        timeout: RUN_INSIGHT_PROCESSOR_TIMEOUT * 10000,
      });
      if (!RUN_INSIGHT_PROCESSOR_LOCK) {
        log.debug('Skipping lock');
        release();
      } else {
        if (!hasLock) {
          log.info('Another instance is already processing insights');
          return;
        } else {
          log.debug('Processing insights due to having lock');
        }
      }

      try {
        if (!insightTask.isConnected()) {
          log.error('Failed to connect to RabbitMQ');
          return;
        }

        let insights = await ums.getInsightToBeProcessed();
        log.debug('Insights to be processed', insights);

        if (insights == null) return;
        if (insights.length === 0) return;

        await ums.acceptProcessedInsight(insights.map(i => i.id));

        const before = new Date(new Date().getTime() - 15 * 60 * 1000); // 15 minutes ago
        const now = new Date();

        insights = insights.filter(i => {
          if (i.frequency === '') {
            log.debug('Insight frequency is empty', i.id);
            return false;
          }
          try {
            const insightCron = parseExpression(i.frequency, CronOptions);
            const lastDate = insightCron.prev().toDate();
            const nextDate = insightCron.next().toDate();
            log.debug('before', before);
            log.debug('nextDate', nextDate);
            if (before <= lastDate && lastDate <= now) {
              log.debug('OK last Date, but skipping');
              return false;
            } else if (before <= nextDate && nextDate <= now) {
              log.debug('OK next Date');
              return true;
            }
            log.debug('NO Date');
            return false;
          } catch (e) {
            log.debug('Failed to parse insight frequency', i.id, i.frequency, e);
            return false;
          }
        });

        log.info(
          'Insight to be processed',
          insights.map(i => i.id),
        );

        for (const insight of insights) {
          insightTask.publishMessage({
            message: JSON.stringify({ insight, force: false }),
            routingKey: 'insight-processor',
            options: {
              expiration: RUN_INSIGHT_PROCESSOR_TIMEOUT * 60 * 1000, // in minutes
            },
          });
        }
      } catch (error) {
        // only release the lock in case there is an issue, so that another pod can retry
        // otherwise, the lock will be released at the end of the timeout, blocking any other pod from running the cron job
        await release();
      }
    },
    start: true,
    timeZone: CronOptions.tz,
  });
};
