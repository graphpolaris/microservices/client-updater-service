import { Logger } from 'ts-common';
import { LOG_LEVEL } from './variables';

export const log = new Logger(LOG_LEVEL as any, 'client-updater-service');
