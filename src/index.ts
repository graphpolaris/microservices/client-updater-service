import { unauthorizedError, wsReturnKey, type WsHandlerMapFlex, type WsMessageBody } from 'ts-common';
import { log } from './logger';
import { DEV, rabbitMq, ROUTING_KEY, QUEUE_NAME, ums, redis } from './variables';
import { getHeadersHandler } from './headers/handler';
import { getSessionDataFromHeader } from './headers/sessionFromHeader';
import type { RabbitPublisherMap, WebSocketData, WsParams } from './model';
import { handleMessage } from './wsMessageProcessors';
import { clientPool, rabbitReader2Frontend } from './wsMsgToFrontend/reader';
import { wsWrite } from './wsMsgToFrontend';
import { startCronJob } from './cronScheduler';
import { RabbitMqBroker } from 'ts-common/rabbitMq';

async function main() {
  log.info('Starting client-updater-service-bun...');
  if (DEV) log.debug('In dev mode');

  log.info('Connecting to Redis...');
  await redis.connect();
  log.info('Connected to Redis!');

  log.info('Connecting to RabbitMQ...');
  const rabbit: RabbitPublisherMap = {
    query: await new RabbitMqBroker(rabbitMq, 'requests-exchange', 'neo4j-query-queue', 'neo4j-query-request').connect(),
    schema: await new RabbitMqBroker(rabbitMq, 'requests-exchange', 'neo4j-schema-queue', 'neo4j-schema-request').connect(),
  };
  const reader = await new RabbitMqBroker(rabbitMq, 'ui-direct-exchange', QUEUE_NAME, ROUTING_KEY).connect();

  if (!rabbit.query.isConnected() || !rabbit.schema.isConnected() || !reader.isConnected()) {
    log.error('Failed to connect to RabbitMQ, retrying in 5 seconds...');
    setTimeout(main, 5000);
    return;
  }
  log.info('Connected to RabbitMQ!');

  rabbitReader2Frontend(reader);

  await startCronJob();

  Bun.serve<WebSocketData>({
    port: 3001,
    async fetch(req, server) {
      const url = new URL(req.url);
      log.info('Request', url.pathname);
      if (url.pathname === '/ws') {
        // upgrade the request to a WebSocket
        log.debug('Upgrading to WebSocket', DEV);
        const { sessionData, foundAll } = await getSessionDataFromHeader(req, !DEV);

        if (!sessionData.username || sessionData.sessionID == null || sessionData.sessionID == '') {
          log.error('No username or sessionID found in header', sessionData);
          return new Response('Unauthorized', { status: 401 });
        }

        // Fix user id, since the one from headers is from authentik which does not match our own id
        const user = await ums.createUserIfNotExistsByName(sessionData.username);
        sessionData.userID = user.id;

        log.debug('Session data', sessionData);
        if (
          server.upgrade(req, {
            data: { sessionData },
          })
        ) {
          log.debug('Upgrade successful');
          return; // do not return a Response
        }
        return new Response('Upgrade failed', { status: 500 });
      } else if (url.pathname === '/headers') {
        // handle the API header request
        return getHeadersHandler(req, redis);
      } else {
        return new Response('Not found', { status: 404 });
      }
    },
    websocket: {
      message(ws, message) {
        log.debug('Received ws message', message, ws.data.sessionData);
        const string = message.toString();

        if (string === 'ping') {
          ws.send('pong', false);
          return;
        }

        const m = JSON.parse(string) as WsMessageBody;
        if (m.key in handleMessage && m.subKey in handleMessage[m.key]) {
          log.debug('Processing message', ws.data.saveStateId);
          // Cast to WsHandlerMapFlex<WsParams> to avoid type error
          try {
            (handleMessage as WsHandlerMapFlex<WsParams>)[m.key][m.subKey](m, { ws, clientPool, redis, rabbit });
          } catch (e) {
            if (e == unauthorizedError) {
              log.error('Unauthorized error', e);
            } else {
              log.error('Error processing message', e);
            }
          }
        } else {
          log.error('Invalid message', m);
        }
      }, // a message is received
      open(ws) {
        clientPool.set(ws.data.sessionData.sessionID, ws);
        log.info('Socket opened', ws.data.sessionData.sessionID);
        wsWrite(ws, {
          callID: '',
          status: 'success',
          type: wsReturnKey.reconnect,
          value: {
            sessionID: ws.data.sessionData.sessionID,
          },
        });
      }, // a socket is opened
      close(ws, code, message) {
        clientPool.delete(ws.data.sessionData.sessionID);
        log.info('Socket closed', ws.data.sessionData.sessionID, code, message);
      }, // a socket is closed
      drain(ws) {
        log.warn('Socket drained');
        // clientPool.set(ws.data.sessionData.sessionID, ws);
      }, // the socket is ready to receive more data
    },
  });
}

await main();
