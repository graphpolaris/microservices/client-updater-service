import { wsReturnKey, type WsMessageBackend2Frontend, type WsMessageData } from 'ts-common';
import type { ServerWebSocketType } from '../model';
import { log } from '../logger';

export const wsWrite = (ws: ServerWebSocketType, data: WsMessageBackend2Frontend) => {
  ws.send(JSON.stringify(data), true);
};

type Optional<T, K extends keyof T> = Pick<Partial<T>, K> & Omit<T, K>;

export const writeJson = (ws: ServerWebSocketType, callID: string, data: Optional<WsMessageBackend2Frontend, 'callID'>) => {
  wsWrite(ws, {
    ...data,
    callID: callID,
  } as WsMessageBackend2Frontend);
};

/**
 * @deprecated This function is deprecated and will be removed in future versions.
 * Please use a more specific function instead.
 */
export const writeRaw = (ws: ServerWebSocketType, callID: string, data: any) => {
  wsWrite(ws, {
    ...data,
    callID: callID,
  } as WsMessageBackend2Frontend);
};

export const writeError = (ws: ServerWebSocketType, callID: string, errorStr: string) => {
  wsWrite(ws, {
    callID: callID,
    status: 'error',
    type: wsReturnKey.error,
    value: errorStr,
  } as WsMessageBackend2Frontend);
};

// TODO below when we reimplement broadcasting
// /*
// Write takes a sessionID and byte slice and sends the data to the correct socket

// 	sessionID: *string, the session ID
// 	data: []byte, the data to be written
// */
// func (p *Pool) WriteToRoom(roomID *string, data *[]byte, emitterID *string) {
// 	roomData, err := p.routingService.GetCache().GetFromRoom(roomID)
// 	if err != nil {
// 		log.Error().Str("roomID", *roomID).Err(err).Msg("error getting room data from redis")
// 		return
// 	}

// 	for _, sessionID := range roomData.Sessions {
// 		if sessionID == *emitterID {
// 			continue
// 		}
// 		client, ok := p.clients.Load(sessionID)
// 		if ok {
// 			clientP := *client.(*socketClient.SocketClient)
// 			clientP.SendChannel <- *data
// 			log.Trace().Str("sessionID", sessionID).Str("roomID", *roomID).Msg("writing to client in room")
// 		} else {
// 			// TODO: need to check the sessions in this client-service, and if not present, need to send back to the message broker to go to the proper client-service
// 			// For now, I'm assuming there is only one client-updater-service, but needs to be fixed
// 			log.Error().Str("sessionID", sessionID).Msg("TODO session was not found, needs to be sent to message broker")
// 		}
// 	}

// 	// room, ok := p.roomClients.Load(roomID)
// 	// if !ok {
// 	// 	log.Error().Str("roomID", *roomID).Msg("room was not found")
// 	// 	return
// 	// }

// 	// log.Info().Str("roomID", *roomID).Msg("writing to room")
// 	// for _, client := range *room.(*map[string]*SocketClient) {
// 	// 	if client.id == *emitterID {
// 	// 		continue
// 	// 	}
// 	// 	log.Trace().Str("sessionID", client.id).Str("roomID", client.room).Msg("writing to client")
// 	// 	// Send the data through the send channel
// 	// 	client.sendChannel <- *data
// 	// }
// }
