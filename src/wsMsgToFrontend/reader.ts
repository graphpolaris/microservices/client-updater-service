import { Logger, type WsMessageBackend2Frontend } from 'ts-common';
import { LOG_LEVEL } from '../variables';
import type { ClientPool, ServerWebSocketType } from '../model';
import { wsWrite } from '.';
import type { RabbitMqBroker } from 'ts-common/rabbitMq';

const log = new Logger(LOG_LEVEL as any, 'client-updater-service');

export const clientPool: ClientPool = new Map<string, ServerWebSocketType>();

export const rabbitReader2Frontend = (reader: RabbitMqBroker) => {
  reader.bindConsumer();
  reader.startConsuming<WsMessageBackend2Frontend>('client-updater-service', async (message, headers) => {
    const sessionID = headers.message.sessionData.sessionID;
    log.info('Received message from RabbitMQ', sessionID, message.type);
    log.debug('Received message from RabbitMQ debug', message, headers, sessionID);
    const client = clientPool.get(sessionID);
    if (client == null) {
      log.error('Client not found in clientPool', sessionID, 'Message', message.type, message.status, message.callID);
      return;
    }

    wsWrite(client, message);
    log.debug('Sent message to client', message, sessionID);
  });
};
